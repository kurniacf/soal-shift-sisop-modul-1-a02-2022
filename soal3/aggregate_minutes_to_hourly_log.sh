#!/bin/bash
 
touch /home/manda/logH/aggregate.log
 
awk -F"," 'BEGIN{a1=100000; b1=100000; c1=100000; d1=100000; e1=100000; f1=100000; g1=100000; h1=100000; i1=100000; k1=100000}
        $1<a1 {a1=$1;} $2<b1 {b1=$2;} $3<c1 {c1=$3;} $4<d1 {d1=$4;} $5<e1 {e1=$5;} $6<f1 {f1=$6;} $7<g1 {g1=$7;} $8<h1 {h1=$8;} $9<i1 {i1=$9;} k1<$11 {k1=$11;}
        END{print"minimum"","a1","b1","c1","d1","e1","f1","g1","h1","i1","$10","k1}' /home/manda/logH/metrics_`date +"%Y%m%d%H"`.log > /home/manda/logH/aggregate.log
 
awk -F"," 'BEGIN{a2=0; b2=0; c2=0; d2=0; e2=0; f2=0; g2=0; h2=0; i2=0; k2=0}
        $1>a2 {a2=$1;} $2>b2 {b2=$2;} $3>c2 {c2=$3;} $4>d2 {d2=$4;} $5>e2 {e2=$5;} $6>f2 {f2=$6;} $7>g2 {g2=$7;} $8>h2 {h2=$8;} $9>i2 {i2=$9;} $11>k2 {k2=$11;}
        END{print"maximum"","a2","b2","c2","d2","e2","f2","g2","h2","i2","$10","k2""}' /home/manda/logH/metrics_`date +"%Y%m%d%H"`.log >> /home/manda/logH/aggregate.log
 
awk -F"," 'BEGIN{a=0; b=0; c=0; d=0; e=0; f=0; g=0; h=0; i=0; k=0}
        {a+=$1; b+=$2; c+=$3; d+=$4; e+=$5; f+=$6; g+=$7; h+=$8; i+=$9; k+=$11}
        END{print "average"","a/NR","b/NR","c/NR","d/NR","e/NR","f/NR","g/NR","h/NR","i/NR","$10","k/NR"""M"}' /home/manda/logH/metrics_`date +"%Y%m%d%H"`.log  >> /home/manda/logH/aggregate.log
 
string2="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available_swap_used,swap_free,path,path_size"
 
echo $string2 > /home/manda/log/metrics_agg_`date +"%Y%m%d%H"`.log && cat /home/manda/logH/aggregate.log >> /home/manda/log/metrics_agg_`date +"%Y%m%d%H"`.log && chmod 700 /home/manda/log/metrics_agg_`date +"%Y%m%d%H"`.log