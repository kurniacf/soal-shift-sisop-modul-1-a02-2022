# Sistem Operasi 2022
Kelas Sistem Operasi A - Kelompok A02
### Nama Anggota 
- Kurnia Cahya Febryanto (5025201073)
- Amanda Salwa Salsabila (5025201172)
- Avind Pramana Azhari (05111940000226)

## Shift 1
### Soal 1
**a.** Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh

</br>

Penyelesaian : 
Membuat file register.sh yang didalamnya terdapat pengecekan isi file dari ./users/user.txt supaya tidak ada username yang sama
```
grep -qi ${username} ./users/user.txt
```
Dan juga di register.sh membuat command simpan username dan password ke ./users/user.txt
```
echo  "${username}"  >> ./users/user.txt
echo  "${user_password}"  >> ./users/user.txt
```
Di sisi lain, pada file main.sh membuat command login supaya tidak ada username dan password yang sama
```
grep -qi ${username} ./users/user.txt && grep -qi ${user_password} ./users/user.txt
```
Jika di main.sh tidak di cek tidak memiliki username yang terdaftar di user.txt, maka langsung di redirect ke register.sh untuk membuat user baru `bash register.sh`
Referensi : [Grep Command](https://github.com/learnbyexample/Command-line-text-processing/blob/master/gnu_grep.md)

</br>

**b.** Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
i) Minimal 8 karakter
ii) Memiliki minimal 1 huruf kapital dan 1 huruf kecil
iii) Alphanumeric
iv) Tidak boleh sama dengan username

</br>

Penyelesaian:
Pada register.sh dilakukan pengecekan terhadap password yang diinputkan. Password harus terhidden saat input dengan cara yaitu sebagai berikut
```
read -r -s user_password
```
Pertama, di cek apakah ada username yang sama dengan password
```
if  [[  ${username}  !=  ${user_password}  ]];  then
```
Setelah itu, di cek apakah password minimal harus 8 character dan berupa alphanumerical. Pengecekan dilakukan menggunakan REGEX
```
if  [[  ${user_password}  =~  [A-Za-z0-9]{8,} ]];  then
```
Selanjutnya, di cek apakah password berupa apakah dalam password memiliki minimal 1 huruf uppercase, 1 huruf lowercase, dan 1 angka numerik. Pengecekan dilakukan menggunakan REGEX
```
if  [[  ${user_password}  =~  [A-Z]  &&  ${user_password}  =~  [a-z]  &&  ${user_password}  =~  [0-9]  ]];  then
```
Referensi: [Bash Regular Expressions](https://www.networkworld.com/article/2693361/unix-tip-using-bash-s-regular-expressions.html), [Regex Editor 101](https://regex101.com/r/EyCDAv/1)

</br>

**c.** Setiap percobaan login dan register akan tercatat pada log.txt dengan format : **MM/DD/YY hh:mm:ss MESSAGE**. Message pada log akan berbeda tergantung aksi yang dilakukan user.

</br>

Penyelesaian:
Setiap aksi yang dilakukan akan tercatat pada file log.txt. Langkah pertama adalah membuat environment variabel mengenai date 
```
date=$(date '+%m-%d-%Y %H:%M:%S')
```
##### Pada file register.sh
- Log untuk user telah tersedia
```
echo  "${date} Error: User Already Exist"  >> log.txt
```
- log untuk password yang sama dengan username
```
 echo  "${date} Error: Password must not same the username"  >> log.txt
```
- log untuk password minimal 8 character dan alphanumerical
```
echo  "${date} Error: Password must constaint minimal 8 characters"  >> log.txt
```
- log untuk password harus minimal 1 huruf uppercase, 1 huruf lowercase, dan 1 angka
```
echo  "${date} Error: Password must constaint from uppercase, lowercase, and numeric"  >> log.txt
```
- log untuk username dan password telah berhasil register 
```
echo  "${date}  ${username} registered successfully"  >> log.txt
```
Referensi : [Date Command in Linux](https://www.geeksforgeeks.org/date-command-linux-examples/#:~:text=date%20command%20is%20used%20to,change%20the%20date%20and%20time.)

</br>

**d.** Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
i. dl N ( N = Jumlah gambar yang akan didownload). Mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai input Hasil download akan dimasukkan ke dalam folder dengan format nama
YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ). Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali.
ii. att
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

</br>

Penyelesaian : 
Langkah pertama buat 2 command menggunakan if else dan membuat environment variabel date_folder sebagai berikut
```
date_folder=$(date '+%Y-%m-%d')_${username}
```
##### Command dl n
Setelah mengetik **dl**, maka inputkan **n**. Setelah itu cek nama date_folder, jika ada maka unzip 
```
unzip ${date_folder}.zip
rm "${date_folder}.zip"
```
Lalu buat folder baru dan mendownload file gambar menggunakan for loop
```
mkdir -p ${date_folder}
for(( i=1; i<=$n; i++))  do
	wget https://loremflickr.com/320/240 -O ${date_folder}/PIC_${i}.jpg
done
```
Setelah itu, di zip kembali menggunakan password
```
zip --password ${user_password} -r ${date_folder}.zip ${date_folder}
```
Kasus lain adalah jika tidak ada nama folder yang sama sebelumnya, maka akan langsung melakukan zip
##### Command att
Pada command att adalah bertujuan untuk menghitung banyak log login dan register yang dilakukan. 
```
awk -v username="$username"  'BEGIN {test=0} $3 == username {test=test+1} END {print (test)}' log.txt
```
menggunakan $3 karena di log.txt yang dibandingkan dan dihitung adalah bagian username dengan contoh
```
02-26-2022 15:55:55 Kurnia registered successfully!
02-26-2022 15:57:42 Kurnia Logged In
02-26-2022 15:59:06 Error: User Already Exist!
02-26-2022 16:00:21 Kurnia Logged In
```
Maka dari contoh tersebut jika dihitung, maka akan menghasilkan att berjumlah 3 buah karena username yang dihitung yaitu username login Kurnia  dan aktivitasnya login dan register

</br>

### Soal 2

File log daffa info akan dicek menggunakan script awk pafa file soal2_forensic_dapos.sh. Pada file log, dapat dilihat data yang berisikan IP, Date, Request, Status cod, sontent length, dan user agent pada setiap baris. Isi data pada file tersebut menggunakan seperator “:” (titik dua) untuk memisahkan data pada masing masing line. Untuk menghitung rata rata request perjam maka kita memerlukan data berupa berapa lama website berjakan dan total request selama website berjalan. Maka akan digunakan script sebagai berikut :

```
hours=`awk -F ":" '$0 !~ /IP/ {count[$3]}; 
END { for (i in count) {k++} print k }' log_website_daffainfo.log`
```
ini berfungsi untuk menghitung berapa jam website berjalan. Pada awk ini, digunakan -F “:” sebagai penanda bahwa “:” merupakan seperatornya. Selanjutnya digunakan $0 !` /IP/ agar line pertama penanda data tidak terhitung. Kemudian count[$3] berfungsi untuk menghitung semua data ke-3 yaitu jam dan dimasukkan kedalam array count. Sehingga kita akan mendapatkan jumlah array  untuk setiap jam yang berbeda. Bagian {for (i in count) {k++} print k} digunakan untuk menghitung jumlah array yang tersimpan. Hasil tersebut kemudian dimasukkan kedalam variable hours untuk melakukan perhitungan selanjutnya. Data ini diambil melalui file log_website_daffainfo_log

```
total=`awk '{total++}; END {print total-1}' log_website_daffainfo.log`
```
kemudian awk akan digunakan lagi untuk menghitung total line dari file untuk menghitung semua request. Hasilnya dimasukkan ke variable total.
```
let rata="$total"/"$hours" 
echo "Rata-rata serangan adalah sebanyak $rata requests per jam" > ./forensic_log_website_daffainfo_log/ratarata.txt
```
setelah itu hasil total dibagi dengan hours menggunakan let dan dimasukkan ke dalam directory ./forensic_log_website_daffainfo_log/ratarata.txt

untuk soal selanjutnya, kita akan mencari ip tdengan request terbanyak dan jumlah request yang dilakukannya. Ini dapat dilakukan menggunakan kode berikut

```
mostip=`awk -F ":" '$0 !~ /IP/ {print $1}' log_website_daffainfo.log |  uniq -c | sort -rn | awk 'NR==1{printf "Ip yang mengakses "$2" sebanyak "$1" kali\n"}'`

echo "$mostip" > ./forensic_log_website_daffainfo_log/result.txt
```
disini akan digunakan 2 awk. Awk pertama untuk melihat semua ip unik dan jumlah reqestnya, dan awk kedua untuk mengambil data dari awk pertama tersebut sebagai input. Pada awk pertama akan dilakukan pipe dengan uniq -c untuk menghitung jumlah input $1 yang unik. Kemudian sort -rn dimana data akan dioutput sebagai dari besar ke kecil menggunakan option -rn. Pada awk kedua, terdapat NR==1 yang berartikan fungsi selanjutnya akan memakai output line pertama dari awk yang pertama. Sehingga $1 menjadi jumlah request dari ip dengan request terbanyak, dan $2 menjadi IP nya. Selanjutnya hasil dimasukkan ke directori /forensic_log_website_daffainfo_log/result.txt.

```
jumcurl=`awk '/curl/ {print $9}' log_website_daffainfo.log | uniq -c`

echo -e "Ada $jumcurl requests yang menggunakan curl sebagai user-agent" >> ./forensic_log_website_daffainfo_log/result.txt
```
pada kode selanjutnya, akan dicari jumlah request yang menggunakan culr sebagi user-agent. Ini dapat dilakukan dengan menggunakan /curl/ agar awk mengambil semua line yang mempunyai curl didalam linenya, dan uniq -c untuk menghitung jumlah line yang didapat. Disini $9 digunakan karena itu merupakan posisi input untuk user-agent. Hasil kemudian dimasukkan kedalam ./forensic_log_website_daffainfo_log/result.txt.

```
jam2=`awk -F ":" '($3 ~ "02" && $2 ~ /22\//){printf ""$1" pada jam "$3":"$4":"$5"\n"}' log_website_daffainfo.log `

echo -e "$jam2" >> ./forensic_log_website_daffainfo_log/result.txt
```
kode selanjutnya digunakan untuk mencari ip yang melakukan request pada jam 2 tanggal 22. ini dilakukan dengan  ($3 ~ "02" && $2 ~ /22\//) dimana $3 ~ “02” akan mencari jam yang stringnya sama dengan 02 dan $2 ~ /22\// mencari string yang mempunyai tanggal 22. hasilnya kemudian dimasukkan ke ./forensic_log_website_daffainfo_log/result.txt



</br>

### Soal 3
File tampungmetrics.log dibuat untuk menampung isi metrics free -m. File tampungmetrics2.log dibuat untuk menampung isi metrics du -sh. File monitoring.log dibuat untuk menampung sementara isi dari log metrics per menit
```
touch tampungmetrics.log
touch tampungmetrics2.log
touch monitoring.log 
```

Isi dari free -m ditampung ke dalam file tampungmetrics.log
```
free -m > tampungmetrics.log
```

Kemudian, metrics dari isi free -m yang telah ditampung di tampungmetrics.log diambil menggunakan awk. Untuk menghindari new line saat dilakukannya print menggunakan awk, digunakan {ORS=","} agar tiap line juga diakhiri dengan tanda koma. Metrics yang telah diambil ditampung sementara ke dalam monitoring.log
```
awk '{ORS=","} FNR == 2{print $2","$3","$4","$5","$6","$7}' tampungmetrics.log > monitoring.log
awk '{ORS=","} FNR == 3{print $2","$3","$4}' tampungmetrics.log >> monitoring.log 
```

Isi dari du -sh /home/manda ditampung ke dalam file tampungmetrics2.log
```
du -sh /home/manda/ > tampungmetrics2.log
```

Path di print dan ditampung sementara ke dalam monitoring.log serta path size yang berada di dalam isi du -sh /home/manda ditampung sementara ke dalam monitoring.log
```
echo -n `pwd` >> monitoring.log
awk '{print ","$1}' tampungmetrics2.log >> monitoring.log 
```

String yang berisi nama-nama dari metrics ditampung di dalam variabel string1
```
string1="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available_swap_used,swap_free,path,path_size" 
```

Isi dari variabel string1 diprint ke dalam metrics_{YmdHms}.log. Di saat bersamaan, isi yang ditampung sementara oleh monitoring.log dimasukan ke dalam file metrics_{YmdHms}.log serta permission file tersebut diubah sehingga hanya user pemilik file yang dapat mengakses dan mengedit file tersebut
```
echo $string1 > /home/manda/log/metrics_`date +"%Y%m%d%H%M%S"`.log && cat monitoring.log >> /home/manda/log/metrics_`date +"%Y%m%d%H%M%S"`.log && chmod 700 /home/manda/log/metrics_`date +"%Y%m%d%H%M%S"`.log 
```

Isi yang ditampung sementara oleh monitoring.log juga ditampung sementara dan ditumpuk ke dalam file metrics_{YmdH}.log per jam agar nantinya bisa dicari nilai minimum, maximum, serta average-nya menggunakan aggregate_minutes_to_hourly_log.sh
```
cat monitoring.log >> /home/manda/logH/metrics_`date +"%Y%m%d%H"`.log
```

File aggregate.log dibuat untuk menampung sementara hasil agregasi dari log metrics per menit yang sudah ditumpuk menggunakan file metrics_{YmdH}.log
```
touch /home/manda/logH/aggregate.log 
```

Pencarian metrics yang memiliki nilai terkecil (minimum) dan terbesar (maximum) serta rata-rata (average) dilakukan menggunakan awk

Untuk minimum, awalnya dibuat 10 variabel yang bernilai 100000. Kemudian nilai dari tiap variabel akan dibandingkan dengan nilai dari tiap metrics. Nilai yang lebih kecil akan ditampung dalam masing-masing variabel dan kemudian diprint kembali untuk menunjukan bahwa nilai tersebut adalah nilai terkecil
```
awk -F"," 'BEGIN{a1=100000; b1=100000; c1=100000; d1=100000; e1=100000; f1=100000; g1=100000; h1=100000; i1=100000; k1=100000}
        $1<a1 {a1=$1;} $2<b1 {b1=$2;} $3<c1 {c1=$3;} $4<d1 {d1=$4;} $5<e1 {e1=$5;} $6<f1 {f1=$6;} $7<g1 {g1=$7;} $8<h1 {h1=$8;} $9<i1 {i1=$9;} k1<$11 {k1=$11;}
        END{print"minimum"","a1","b1","c1","d1","e1","f1","g1","h1","i1","$10","k1}' /home/manda/logH/metrics_`date +"%Y%m%d%H"`.log > /home/manda/logH/aggregate.log
```

Untuk maximum, awalnya dibuat 10 variabel yang bernilai 0. Lalu nilai dari tiap variabel akan dibandingkan dengan nilai dari tiap metrics. Nilai yang lebih besar akan ditampung dalam masing-masing variabel dan kemudian diprint kembali untuk menunjukan bahwa nilai tersebut adalah nilai terbesar
```
awk -F"," 'BEGIN{a2=0; b2=0; c2=0; d2=0; e2=0; f2=0; g2=0; h2=0; i2=0; k2=0}
        $1>a2 {a2=$1;} $2>b2 {b2=$2;} $3>c2 {c2=$3;} $4>d2 {d2=$4;} $5>e2 {e2=$5;} $6>f2 {f2=$6;} $7>g2 {g2=$7;} $8>h2 {h2=$8;} $9>i2 {i2=$9;} $11>k2 {k2=$11;}
        END{print"maximum"","a2","b2","c2","d2","e2","f2","g2","h2","i2","$10","k2""}' /home/manda/logH/metrics_`date +"%Y%m%d%H"`.log >> /home/manda/logH/aggregate.log
```

Untuk average, awalnya dibuat 10 variabel yang bernilai 0. Lalu nilai dari tiap metrics akan ditampung dan dijumlahkan ke dalam tiap variabel. Jumlah nilai metrics dalam tiap variabel akan dibagi dengan nilai NR (jumlah line yang ada) agar nilai rata-ratanya dapat dicari dan kemudian diprint kembali
```
awk -F"," 'BEGIN{a=0; b=0; c=0; d=0; e=0; f=0; g=0; h=0; i=0; k=0}
        {a+=$1; b+=$2; c+=$3; d+=$4; e+=$5; f+=$6; g+=$7; h+=$8; i+=$9; k+=$11}
        END{print "average"","a/NR","b/NR","c/NR","d/NR","e/NR","f/NR","g/NR","h/NR","i/NR","$10","k/NR"""M"}' /home/manda/logH/metrics_`date +"%Y%m%d%H"`.log  >> /home/manda/logH/aggregate.log
```

String yang berisi type dan nama-nama dari metrics ditampung di dalam variabel string2
```
string2="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available_swap_used,swap_free,path,path_size" 
```

Isi dari variabel string2 diprint ke dalam metrics_agg_{YmdH}.log. Di saat bersamaan, isi yang ditampung sementara oleh aggregate.log dimasukan ke dalam file metrics_agg_{YmdH}.log serta permission file tersebut diubah sehingga hanya user pemilik file yang dapat mengakses dan mengedit file tersebut
```
echo $string2 > /home/manda/log/metrics_agg_`date +"%Y%m%d%H"`.log && cat /home/manda/logH/aggregate.log >> /home/manda/log/metrics_agg_`date +"%Y%m%d%H"`.log && chmod 700 /home/manda/log/metrics_agg_`date +"%Y%m%d%H"`.log
```

Kemudian file metrics_`date +"%Y%m%d%H%M%S"`.log dijalankan tiap menit menggunakan crontab dengan perintah
```
* * * * * /home/manda/log/metrics_`date +"%Y%m%d%H%M%S"`.log 
```

Dan terakhir, file metrics_agg_`date +"%Y%m%d%H"`.log dijalankan tiap jam menggunakan crontab dengan perintah
```
00 * * * * /home/manda/log/metrics_agg_`date +"%Y%m%d%H"`.log
```

### Revisi
#### Soal 1
Pada soal 1, melakukan revisi pada command
```
wget https://loremflickr.com/320/240 -O ${date_folder}/PIC_${i}.jpg
```
yang sebelumnya file hasil download tidak masuk dan tersimpan dalam zip. Maka revisi dilakukan dengan menambah `-O` sehingga file hasil download bisa tersimpan dalam zip. Revisi selanjutnya yaitu pada command 
```
zip --password ${user_password} -r ${date_folder}.zip ${date_folder}
```
yang sebelumnya tidak ada fitur password, sehingga dilakukan dengan menambahkan `--password`

#### Soal 2

#### Soal 3
Aman, tidak ada revisi

### Kendala
Masih perlu mempelajari banyak command yang sederhana tetapi memiliki pengaruh besar beserta fungsinya seperti 
```
-O , -p, -e, -l, -s, -f, -r, -rv, dan lain-lain
```
