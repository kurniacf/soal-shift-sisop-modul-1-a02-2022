#!/bin/bash


#b
hours=`awk -F ":" '$0 !~ /IP/ {count[$3]}; 
END { for (i in count) {k++} print k }' log_website_daffainfo.log`

total=`awk '{total++}; END {print total-1}' log_website_daffainfo.log`


let rata="$total"/"$hours" 
echo "Rata-rata serangan adalah sebanyak $rata requests per jam" > ./forensic_log_website_daffainfo_log/ratarata.txt


#c
mostip=`awk -F ":" '$0 !~ /IP/ {print $1}' log_website_daffainfo.log |  uniq -c | sort -rn | awk 'NR==1{printf "Ip yang mengakses "$2" sebanyak "$1" kali\n"}'`

echo "$mostip" > ./forensic_log_website_daffainfo_log/result.txt

#d
jumcurl=`awk '/curl/ {print $9}' log_website_daffainfo.log | uniq -c`

echo -e "Ada $jumcurl requests yang menggunakan curl sebagai user-agent" >> ./forensic_log_website_daffainfo_log/result.txt

#e
jam2=`awk -F ":" '($3 ~ "02" && $2 ~ /22\//){printf ""$1" pada jam "$3":"$4":"$5"\n"}' log_website_daffainfo.log `

echo -e "$jam2" >> ./forensic_log_website_daffainfo_log/result.txt


#> ./forensic_log_website_daffainfo_log/ratarata.txt

#test awk -F ":" '$0 !~ /IP/ {print $6}' log_website_daffainfo.log | sort | uniq -c | sort -rn

