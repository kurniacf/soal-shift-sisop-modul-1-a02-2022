#!/bin/bash

read username
read -r -s user_password
date=$(date '+%m-%d-%Y %H:%M:%S')
date_folder=$(date '+%Y-%m-%d')_${username}

if grep -qi ${username} ./users/user.txt && grep -qi ${user_password} ./users/user.txt ; then
    echo "Login Mode"
    echo "${username} Logged In!"
    echo "${date} ${username} Logged In!" >> log.txt

    echo "Please fill the command dl/att : "
    read command

    if [[ ${command} == dl ]] ; then
        echo "Input n: "
        read n

        if [[ -f "${date_folder}.zip" ]] ; then
            echo "Unzip Mode"
            unzip ${date_folder}.zip
            rm "${date_folder}.zip"
        fi

        echo "Zip Mode"
            mkdir -p ${date_folder}
            for(( i=1; i<=$n; i++)) do
                wget https://loremflickr.com/320/240 -O ${date_folder}/PIC_${i}.jpg
            done
            zip --password ${user_password} -r ${date_folder}.zip ${date_folder}
            rm -r ${date_folder}

    elif [[ ${command} == att ]] ; then
        awk -v username="$username" 'BEGIN {test=0} $3 == username {test=test+1} END {print (test)}' log.txt
    else
        echo "Error: Command is wrong"
    fi
else
    echo "Error: Failed to Login because has not"
    echo "Error: Failed to Login because has not" >> log.txt
    echo "do not have an account, will be moved to register mode"
    bash register.sh
fi